 <meta name="auteur" content="Nicolas Rochet">
 <meta name="date_creation" content="mercredi, 01. avril 2020">
 <meta name="date_modification" content="mercredi, 01. avril 2020">

# Projet 1: Analyse de sentiment multi-langue

## Description du sujet

Dans ce projet, votre tâche consiste à construire un modèle capable de **classer les sentiments**  de données textuelles, au **moins sur deux niveaux** (positif, négatif) et capable de fonctionner à **minima sur des textes en anglais et en français**.

## Modèles

Vous êtes libre d'utiliser toutes les méthodes et modèles que vous souhaitez, mais vous devez utiliser **au moins un modèle de deep learning**.
Vous devrez faire des recherches pour identifier les modèles et méthodes qui sont pertinentes pour votre tâche. Je vous encourage à utiliser toutes les ressources disponibles dans le cours, le lexique et la section ressources de ce documents. N'hésitez pas à me solliciter pour avoir de l'aide sur les questions que vous vous posez.

## Données

Vous êtes libre de constituer le corpus de données qui vous semblera le plus adapté pour votre tâche en fonction des ressources qui vous seront fournies et de vos recherches.

\- ***sujet :*** Vous êtes libre de choisir le sujet de votre choix. Voici une suggestion de sujet d’actualités sur lesquels vous trouverez assez facilement des données :

## Rendu & évaluations

### notation
Vous serez évalué en deux parties:
- Tout d'abord vous travaillerez à construire un modèle capable de classer les sentiments avec les meilleures performances sur n'importe **quel texte en anglais**.  Cette partie comptera pour  **2/3 de la note totale (soit 14 points)**
- Dans la seconde partie, vous tenterez d'améliorer votre modèle pour qu'il soit capable de  classer les sentiments sur des textes **d'autres langues, incluant à minima le français**. Cette partie comptera pour **1/3 de la note totale (soit 6 points)**.
J'accorderais jusqu'à 5 points de bonus aux équipes qui auront un construit et testé un modèle sur le plus de langues possibles !

Votre note finale sera calculée ainsi : $$note~finale = 2/3 * note~partie1 + 1/3 * note~partie2$$

### critères d'évaluations:
Pour chaque partie vous serez évalué sur deux critères, chacun noté sur 20:
- la performance de votre modèle: Suivant une ou plusieurs métriques adaptée, commentez la performance de votre modèle. Dans quel cas fait il des erreurs ? Pouvez vous donnez des exemples de cas caractéristiques ? Quels sont les axes d'améliorations que vous pourriez apporter si vous aviez plus de temps ? ...
- la qualité de vos explications:  Quels jeux de données avez vous utilisé ? Quels traitements avez vous faits ? Quels modèles ? ...
J'attends que vous **commentiez et critiquez votre approche** !

### livrables
Vous devez rendre **notebook jupyter (au format.pynb)** exposant vos différentes étapes de traitement, le code utilisé, et vos commentaires et conclusions. 
En pratique, vous utiliserez probablement des environnements de développement différent (Spyder, PyCharm, ...), mais vous devrez regoupez votre code dans le notebook pour avoir un rendu final de qualité. Faites en sorte  **que je puisse ré-exécuter si besoin** dans un temps acceptable, ne mettez donc pas de blocs de code qui durent des heures à exécuter !. Je vous conseille donc de sauvegarder vos modèles une fois entrainés et de les recharger à chaque fois que vous en aurez besoin. Concernant le code que vous ne ferez pas appraitre dans le notebook (comme par ex),  pensez à conserver des traces de ces traitements (par exemple des copies d'écran de votre apprentissage)

## Ressources pour vous aider

### Mots clés
Voici les mots clés qui vous seront utiles dans vos recherches concernant les modèles qui pourraient vous être utiles.
- Recurrent Neural Network (RNN) 
- Long Short Term Memory network (LSTM)
- Gated Recurrent Neural network (GRU)
- Word embedding

### Théorie & Modèles
Vous trouverez assez facilement sur kaggle ou des blogs de data science des tutoriels expliquant à la fois la théorie et des exemples de code pour cette tâche. Par exemple:
- [Cet article sur kaggle](https://www.kaggle.com/bertcarremans/deep-learning-for-sentiment-analysis) qui détaille une démarche d'analyse un simple perceptron multicouche sur des données extrait de tweets 
- [Un article du blog towards datascience](https://towardsdatascience.com/sentiment-analysis-with-deep-learning-62d4d0166ef6) très généraliste expliquant une démarche d'analyse de sentiment utilisant des RNN à partir de données de revues de Netflix
- (Cet article expliquant le modèle DeepMoji](https://medium.com/huggingface/understanding-emotions-from-keras-to-pytorch-3ccb61d5a983) pour l'analyse de sentiments (à partir d'extraction d'émojis) avec des LSTM sous PytTorch
- ...

### Frameworks
Tout au long de ce projet, vous apprendrez à vous familiarisez avec les frameworks Tensorflow et PyTorch. Je vous conseille d'aller explorer leur section tutoriels, en particulier sur des exemples qui pourraient vous servir dans cette tâche, par exemple:
- [la classification de noms sous PyTorch](https://pytorch.org/tutorials/intermediate/char_rnn_classification_tutorial.html) utilisant des RNN 
- [la classification de texte sous Tensorflow](https://www.tensorflow.org/tutorials/text/text_classification_rnn) utilisant des RNN 
- [les word embeddings](https://www.tensorflow.org/tutorials/text/word_embeddings) sous Tensorflow

### Données 
Voici plusieurs catalogue de données qui pourraient vous être utiles:
- [le catalogue de données de Tensorflow](https://www.tensorflow.org/datasets/catalog/overview) avec en particulier les datasets [amazon reviews](https://www.tensorflow.org/datasets/catalog/amazon_us_reviews) ou [yelp polarity reviews](https://www.tensorflow.org/datasets/catalog/yelp_polarity_reviews)

- [le catalogue de données textuelles de PyTorch](https://github.com/pytorch/text#datasets)

- [le portail de datasets de Kaggle](https://www.kaggle.com/datasets)

- [le portail de datasets pour le machine learning de l'UCI](https://archive.ics.uci.edu/ml/index.php)

- ...

  Enfin vous pouvez assez facilement extraire des textes sur des sujets polémiques en utilisant des méthodes de scraping à partir de sites comme twitter ou de reddit par exemple.

Bon courage à tous !





